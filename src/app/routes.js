module.exports = (app, passport) => {

	
	// index routes
	app.get('/', (req, res) => {
		res.render('index');
	});

	//************** USER CONTROLLER ROUTES ************************
	const user_controller = require('./controllers/user.js');
	// Create a new user
	app.post('/user', user_controller.create);

	// Retrieve all user
	app.get('/user', user_controller.findAll);

	// Retrieve a single user with userId
	app.get('/user/:userId', user_controller.findOne);

	// Update a User with userId
	app.put('/user/:userId', user_controller.update);

	// Delete a User with userId
	app.delete('/user/:userId', user_controller.delete);
	//************** USER CONTROLLER ROUTES ************************

	//************** VIDEOGAME CONTROLLER ROUTES ************************
	const videogame_controller = require('./controllers/videogame.js');
	// Create a new videogame
	app.post('/videogame', videogame_controller.create);

	// Retrieve all videogame
	app.get('/videogame', videogame_controller.findAll);

	// Retrieve a single videogame with videogameId
	app.get('/videogame/:videogameId', videogame_controller.findOne);

	// Update a videogame with videogameId
	app.put('/videogame/:videogameId', videogame_controller.update);

	// Delete a videogame with videogameId
	app.delete('/videogame/:videogameId', videogame_controller.delete);
	//************** VIDEOGAME CONTROLLER ROUTES ************************

	//login view
	app.get('/login', (req, res) => {
		res.render('login.ejs', {
			message: req.flash('loginMessage')
		});
	});

	app.post('/login', passport.authenticate('local-login', {
		successRedirect: '/profile',
		failureRedirect: '/login',
		failureFlash: true
	}));

	// signup view
	app.get('/signup', (req, res) => {
		res.render('signup', {
			message: req.flash('signupMessage')
		});
	});

	app.post('/signup', passport.authenticate('local-signup', {
		successRedirect: '/profile',
		failureRedirect: '/signup',
		failureFlash: true // allow flash messages
	}));

	//profile view
	app.get('/profile', isLoggedIn, (req, res) => {
		res.render('profile', {
			user: req.user
		});
	});

	// logout
	app.get('/logout', (req, res) => {
		req.logout();
		res.redirect('/');
	});
};

function isLoggedIn (req, res, next) {
	if (req.isAuthenticated()) {
		return next();
	}

	res.redirect('/');
}
